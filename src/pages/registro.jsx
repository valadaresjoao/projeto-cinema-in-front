import { useState } from "react";
import { Footer } from "../layouts/Footer";
import { Header } from "../layouts/Header";
import styles from "../layouts/registro.module.css";
export function Registro() {
  const [nome, setNome] = useState("");
  const [sobrenome, setSobrenome] = useState("");
  const [cpf, setCpf] = useState("");
  const [dataDeNascimento, setdataNascimento] = useState("");
  const [nomeDeUsuario, setNomeUsuario] = useState("");
  const [email, setemail] = useState("");
  const [senha, setsenha] = useState("");
  const [confirmarSenha, setconfirmarSenha] = useState("");

  const [usuario, setUsuario] = useState();

  function modalClose(){
    const modal = document.querySelector('dialog')
    modal.close()
    window.location.assign(`http://localhost:5173/Login`);

  }
  function handleClick(e) {
    e.preventDefault();
    if(nome.length && sobrenome.length && cpf.length && dataDeNascimento.length && nomeDeUsuario.length && email.length && senha.length && confirmarSenha.length > 0){
    fetch("http://localhost:8000/user/cadastro", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        nome,
        sobrenome,
        cpf,
        dataDeNascimento,
        nomeDeUsuario,
        email,
        senha,
        confirmarSenha,
      }),
    })
      .then((resp) => resp.json())
      .then((data) => {
        setUsuario(data)
        console.log(data);
      })
      .catch((err) => console.log(err))
      .catch((err) => console.log(err));
      const modal = document.querySelector('dialog')
      modal.showModal()
    }
    else{
      alert('Todos campos devem ser preenchidos')
      {modalRegister.showModal()}
    }
  }
  return (
    <section>
      <Header usuario={usuario} />
      <main className={styles.mainRegistro}>
        <h1>Junte-se à Comunidade Cinematográfica! Cadastre-se Aqui!</h1>
        <h5>
          Seja bem-vindo à nossa comunidade apaixonada pelo mundo do cinema. Ao
          fazer parte do nosso espaço digital, você está prestes a mergulhar em
          uma experiência cinematográfica única, onde a magia das telonas ganha
          vida com um toque moderno. Nosso formulário de cadastro é o primeiro
          passo para embarcar nessa jornada emocionante. Ao preenchê-lo, você se
          tornará um membro da nossa comunidade, onde amantes do cinema se
          reúnem para compartilhar o entusiasmo, as emoções e as histórias que
          permeiam cada cena.
        </h5>
        <div className={styles.form}>
          <h1 className={styles.titleRegistro}>Registre-se</h1>
          <input
            required
            type="text"
            placeholder="Nome"
            onChange={(e) => setNome(e.target.value)}
            className={styles.inputRegister}
          />
          <input
            required
            type="text"
            onChange={(e) => setSobrenome(e.target.value)}
            placeholder="Sobrenome"
            className={styles.inputRegister}
          />
          <input
            required
            type="text"
            onChange={(e) => setCpf(e.target.value)}
            placeholder="CPF"
            className={styles.inputRegister}
            maxLength={11}
          />
          <input
            required

            onChange={(e) => setdataNascimento(e.target.value)}
            type="date"
            placeholder="Data de Nascimento"
            className={styles.inputRegister}
          />
          <input
            required
            type="text"
            onChange={(e) => setNomeUsuario(e.target.value)}
            placeholder="Nome de Usuário"
            className={styles.inputRegister}
          />
          <input
            required
            type="text"
            onChange={(e) => setemail(e.target.value)}
            placeholder="E-Mail"
            className={styles.inputRegister}
          />
          <input
            required
            onChange={(e) => setsenha(e.target.value)}
            type="text"
            placeholder="Senha"
            className={styles.inputRegister}
          />
          <input
            required
            onChange={(e) => setconfirmarSenha(e.target.value)}
            type="text"
            placeholder="Confirmar Senha"
            className={styles.inputRegister}
          />
          <button
            onClick={(e) => handleClick(e)}
            className={styles.btnRegistrar}
          >
            REGISTRAR
          </button>
          
        </div>
          <dialog className={styles.modalRegister}>
            <div className={styles.positionItemsModal}>
              <div className={styles.modalText}>
                <h2>Cadastro Criado!</h2>
                <h4>Bem-Vindo à Nossa Comunidade Cinematográfica!</h4>
                <p>Obrigado por se juntar a nós na nossa comunidade cinematográfica.Sua jornada para uma experiência cinematográfica única começa agora.
                </p>
                
                <p>
                Você será redirecionado em instantes para página de login em instantes.
                </p>
              </div>
              <button onClick={modalClose}></button>

            </div>
            
          </dialog>
      </main>
      <Footer />
    </section>
  );
}
