import "./Footer.css";
import icone_Instagram from "../Assets/Icone-Instagram.svg";
import icone_Facebook from "../Assets/Icone-Facebook.svg";
import icone_Linkedin from "../Assets/Icone-Linkedin.svg";
import IconeIngresso from "../Assets/INgresso.png";

export function Footer() {
  return (
    <footer>
      <section className="footer_section">
        <div className="footer_infos">
          <div>
            <div className="endereco">
              <h3>Endereço</h3>
              <h5>
                Av. Milton Tavares de Souza, s/n - Sala 115 B - Boa Viagem,
                Niterói - RJ CEP: 24210-315
              </h5>
            </div>

            <div className="fale">
              <h3>Fale conosco</h3>
              <h5>contato@injunior.com.br</h5>

              <div className="icons">
                <img src={icone_Instagram}></img>
                <img src={icone_Facebook}></img>
                <img src={icone_Linkedin}></img>
              </div>
            </div>
          </div>
        </div>

        <div>
          <img src={IconeIngresso} alt="ingresso" className="ingresso"></img>
        </div>

        <iframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.2002885680436!2d-43.134794253045335!3d-22.90598221163769!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x99817ed79f10f3%3A0xb39c7c0639fbc9e8!2sIN%20Junior%20-%20Empresa%20Junior%20de%20Computa%C3%A7%C3%A3o%20da%20UFF!5e0!3m2!1spt-BR!2sbr!4v1692526892275!5m2!1spt-BR!2sbr"
          width="290"
          height="220"
          loading="lazy"
        ></iframe>
      </section>

      <section className="direitos">
        <h5>
          © Copyright 2023. IN Junior. Todos os direitos reservados. Niterói,
          Brasil.
        </h5>
      </section>
    </footer>
  );
};
