import "./MoviesReady.css";
import {NavLink} from 'react-router-dom'

import image_movie1 from "../../Assets/besouroAzul.webp";



const MoviesReady = (props) => {
  console.log(props.dado);

  function handleRedirect(e, id) {
    e.preventDefault();
    console.log(id);
    window.location.assign(`http://localhost:5173/MostrarSessoesTESTEjs/${id}`);
  }

  return (
    <section className="movies_cartaz">
      <h1>Em cartaz</h1>

      <div className="movies">
        {props.dado.map((filme) => {
          return (
            <div className="individual_movie" key={filme.id}>
              <img src={filme.urlImagem} alt="besouro azul" width="250"></img>
              <h4>{filme.titulo}</h4>
              <a href="#">
                <button onClick={(e) => handleRedirect(e, filme.id)}>
                  SESSÕES DISPONÍVEIS
                </button>
              </a>
            </div>
          );
        })}
      </div>
      <div className="ver_mais">
        <NavLink to="/Filmes" className="linkToFilmes">
          <button className="btnSeeMore">Ver mais</button>
        </NavLink>
      </div>
    </section>
  );
};

export default MoviesReady;
