import { Footer } from "../layouts/Footer";
import { Header } from "../layouts/Header";
import IconeIngresso from "../Assets/Icone-Ingresso.svg";
import styles from "../layouts/login.module.css"
import { useState } from "react";
import { NavLink} from 'react-router-dom'
export function Login(){
    
    const cinema = 'C{IN}EMA'
    const [atualUser, setUser] = useState("")
    const [password, setPassword] = useState("")
    
    function loginSubmit(){
        const userlogin = {
            atualUser,
            password
        }
        console.log(userlogin)
        const fetchConfig = {
            "method": "GET",
             headers: {
                "Content-Type": "application/json",
              }
        }
        fetch("http://localhost:8000/user/login",fetchConfig)
        .then((resposta) => {
            resposta.json()
            .then((resposta) => {
                console.log(resposta)
                let i = 0
                while(i <= resposta.length){
                if(atualUser.toUpperCase() == resposta[i].nomeDeUsuario.toUpperCase() || atualUser == resposta[i].email){
                    if(resposta[i].senha === password){
                    window.location.assign(`http://localhost:5173/`);
                    } 
                    break
                }
                else{
                    i++
                    console.log(i)

                    continue
                }
            }
            })
            .catch((error) => {
                console.log(error)
            })
        })
        .catch((error) => {
            console.log(error)
        })
    }

    return(
    <section>
        <Header/>
            <main className={styles.mainLogin}>
                
                <div className={styles.logoPosition}>
                    <div className={styles.imageLogo}>
                        <img src={IconeIngresso} alt="ingresso" className={styles.logoIcon}></img>
                        <h1 className={styles.logoText}>{cinema}</h1>
                    </div>
                    <h5 className={styles.loginDescription}>Escolha suas sessões, reserve seus lugares e entre de cabeça em narrativas que cativam e emocionam. Este é o nosso convite para você vivenciar o cinema de maneira única. Nossa página de login é a porta de entrada para essa experiência excepcional, e estamos empolgados para compartilhar cada momento cinematográfico com você.</h5>
                </div>

                <div className={styles.loginDescriptionAndForm}>
                        <div className={styles.formLogin} >
                            <h2 className={styles.loginSubtitle}>Login</h2>
                            <h5 className={styles.loginInstruction}>Faça login e garanta o seu lugar na diversão</h5>
                            <form action="">
                                <input type="text" className={styles.inputLogin} placeholder='Usuário ou E-Mail' onChange={(e) => setUser(e.target.value)} required/>
                                
                                <input 
                                type="text" 
                                className={styles.inputLogin} 
                                placeholder='Senha' 
                                onChange={(e) => setPassword(e.target.value)} 
                                required/>
                                
                                <button type='submit' 
                                className={styles.btnSendLogin} 
                                onClick={(e)=>{loginSubmit()}}>
                                ENTRAR</button>
                            </form>
                            <div className={styles.loginLine}></div>
                            <NavLink to='/Registro'><button className={styles.btnCadaster}>CADASTRE-SE</button></NavLink>
                            
                        </div>
                </div>
            </main>
        <Footer/>
    </section>
    )
}