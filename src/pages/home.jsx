// import { json } from "express";
import "../layouts/home.module.css";
import Main from "../components/Main";
import { useState, useEffect } from "react";
import { Header } from "../layouts/Header";
import { Footer } from "../layouts/Footer";
// import axius from "axius";

function Home() {
  const [filmes, setFilmes] = useState([]);

  useEffect(() => {
    fetch("http://localhost:8000/movie", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((resp) => resp.json())
      .then((data) => {
        const newData = [];
        for (let i = 0; i < data.length ; i++) {
          newData.push(data[i]);
        }
        const randomFilmes = [];
        const filmesSelecionados = [];
        while (randomFilmes.length < 5){
          const randomIndex = Math.floor(Math.random() * newData.length)
          if(filmesSelecionados.includes(randomIndex)){
            continue
          }
          else {
            filmesSelecionados.push(randomIndex)
            randomFilmes.push(newData[randomIndex])
          }
        }
        console.log(newData);
        setFilmes(randomFilmes);
      })
      .catch((err) => console.log(err))
      .catch((err) => console.log(err));
  }, []);

  return (
    <div className="App">
      <Header />
      <Main dado={filmes}></Main>
      <Footer />
    </div>
  );
}

export default Home;
