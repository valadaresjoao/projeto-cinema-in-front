import { register } from 'swiper/element/bundle';

register();


import "swiper/css";

import "swiper/css/navigation"
import "swiper/css/pagination";
import "swiper/css/scrollbar";


import ticket1 from "../assets/Ticket1.png"
import ticket2 from "../assets/Ticket2.png"
import ticket3 from "../assets/Ticket3.png"
import styles from '../layouts/slider.module.css'

import { Swiper, SwiperSlide} from "swiper/react"

const data = [
    { id: 1, image: ticket1},
    { id: 2, image: ticket2},
    { id: 3, image: ticket3}
]



export function Slider(){
    return(
        <Swiper
        slidesPerView={1}
        navigation={true}
        loop={true}
        className={styles.slide_container}
        >
            {data.map((item) => (
            <SwiperSlide key={item.id} className={styles.swipe}>
                <img src={item.image} alt="slider" className={styles.slide_item}/>
                
            </SwiperSlide>
            ))}

        </Swiper>
    )
}