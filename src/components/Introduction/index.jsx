import './Introduction.css'

const Introduction = (props)=>{

    return(
        <section className='introduction_section'>
            <div className="introduction_titles">
                <div className='introduction_text'>
                    <h1>Transformando Filmes em Experiências Personalizadas</h1>
                </div>
                <div className='introduction_text'>
                    <h2>Reserve Seu Assento e Viva a Magia do Cinema!</h2>
                </div>
            </div>
        </section>
    )

}

export default Introduction


