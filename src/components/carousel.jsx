import styles from '../layouts/carousel.module.css'
import card from '../assets/Card-Ingresso.svg'
import ticket from "../assets/Ticket1.png"
import ticket1 from "../assets/Ticket2.png"
import ticket2 from "../assets/Ticket3.png"
import toLeft from "../assets/Vectorleft.svg"
import toRight from "../assets/VectorRigth.svg"
import popPromo from "../assets/PromopopPromo.svg"

import { createElement, useState, useRef, useEffect } from 'react'
var slideNumber = 0
export function Carousel(){
    const classTwo = 'currentItem'
    const carousel = useRef(null)
    const element = useRef(null)
    const slideToRight = (e) => {
        if(slideNumber <= 2){
        slideNumber++
        e.preventDefault();
        carousel.current.scrollLeft += element.current.offsetWidth
    }}
    
    const slideToLeft = (e) => {
        if(slideNumber > 0){
            slideNumber--
        e.preventDefault();
        carousel.current.scrollLeft -= element.current.offsetWidth
    }}

    return(
        <div className={styles.diminuir}>
        

            <div className={styles.container}>
                <button className={styles.buttonToLeft} onClick={slideToLeft}></button>
                <div className={styles.gallery}>
                    <div className={styles.galleryWrapper}  ref={carousel}>
                                <img src={ticket} alt="" />
                                <img src={ticket1} alt="" ref={element}/>
                                <img src={ticket2} alt="" />
                    </div>
                    

                </div>
                <button className={styles.buttonToRight} onClick={slideToRight}></button>   
            </div>

            <div className={styles.buttonsAndCarousel}>
            </div>  

        </div>
               
    )
}