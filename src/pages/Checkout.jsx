import { useEffect, useState } from "react";
import "../layouts/Checkout.css";
import { Footer } from "../layouts/Footer";
import { Header } from "../layouts/Header";
import { useParams } from "react-router-dom";
import imagecadeira from "../../public/Vector.png"

export function Checkout() {

  const { sessaoId, filmeId, tipo, horario } = useParams()
  const [filme, setFilme] = useState([])
  const [assentos, setAssentos] = useState([]);
  const [assento, setAssento] = useState([]);
  const [assentosSelecionados, setAssentosSelecionados] = useState([]);

  const [nome, setNome] = useState("")
  const [cpf, setCpf] = useState("")

  const [cpfNumero, setCpfNumero] = useState("")
  const [nomeLetra, setNomeLetra] = useState("")

  function mostarAssentos() {
    if (assento.length >= 1) {
      // return ()
    }
  }

  function modalCheckoutActivate(e) {
    e.preventDefault()
    // console.log(nomeLetra)
    // console.log(cpfNumero)
    // // setnome([...nome, nomeLetra])
    // // setCpf([...cpf, cpfNumero])

    // let nomeVazio = false
    // let cpfVazio = false

    // nome.forEach(nomeString => {
    //   console.log(nomeString)
    //   if (!nomeString) {
    //     nomeVazio = true
    //   }
    // });

    // cpf.forEach(cpfString => {
    //   console.log(cpfString)

    //   if (!cpfString) {
    //     cpfVazio = true
    //   }
    // });
    // if(nomeVazio || cpfVazio){
    //   alert("Campos obrigatórios não podem ser vazios.")
    // }
    // else{
    //   const modalCheckout = document.querySelector('.modalCheckout')

    // }
    const modalCheckout = document.querySelector('.modalCheckout')
    const valid = assentosSelecionados.filter((assento) => assento.cpfOcupante && assento.nomeOcupante)
    if (valid.length == assentosSelecionados.length) {
      modalCheckout.showModal();
    }
  }

  function cpfSetados(cpfNumero) {
    setCpfNumero(cpfNumero)
  }

  function mostarCadastro(e) {
    e.preventDefault()
    console.log(nome)
    console.log(cpf)
    // const check = assentos.filter((assento) =>{
    //   assento.id == assentos.id

    // })
    // console.log(check)
    // console.log(e.target)
    // let button = e.target
    // button.classList.add('verBtn')
    // // button.classList.remove('verBtn')
  }

  function nomesSetados(nomeLetra) {
    setNomeLetra(nomeLetra)
  }
  // if (nome.length > 0 && cpf.length > 0) {
  // const modalCheckout = document.querySelector('.modalCheckout')
  // modalCheckout.showModal()
  // } else {
  // alert("preencha todos os campos")
  // }
  function modalCheckoutClose() {
    const modalCheckout = document.querySelector('.modalCheckout')
    modalCheckout.close()
  }

  function modalCheckoutConcluded() {
    const modalConcluded = document.querySelector('.modalConcluded')
    modalConcluded.showModal()
    const closeModal = document.querySelector('.modalCheckout')
    closeModal.close()
  }
  function modalCheckoutConcludedClose() {
    const modalConcluded = document.querySelector('.modalConcluded')
    modalConcluded.close()
  }

  function checkAssentos(assentoid) {

  }

  useEffect(() => {
    fetch(`http://localhost:8000/movie/${filmeId}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((resp) => resp.json())
      .then((data) => {

        setFilme(data);
      })
      .catch((err) => console.log(err))
      .catch((err) => console.log(err));


    fetch(`http://localhost:8000/seats/${sessaoId}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((resp) => resp.json())
      .then((data) => {
        setAssentos(data);
      })
      .catch((err) => console.log(err))
      .catch((err) => console.log(err));
  }, [])

  function verTipo(tipo) {
    if (tipo == 0) {
      return "2D";
    } else if (tipo == 1) {
      return "3D";
    } else {
      return "IMAX"
    }
  }




  function verassentos() {
    console.log(assentos)
  }
  // function handleAssento(e, idAssento, idSessao) {
  //   e.preventDefault()

  //   fetch(`http://localhost:8000/seats/${idSessao}/${idAssento}`, {
  //     method: "GET",
  //     headers: {
  //       "Content-Type": "application/json",
  //     },
  //   })
  //     .then((resp) => resp.json())
  //     .then((data) => {
  //       setAssento([...assento, data]);
  //     })
  //     .catch((err) => console.log(err))
  //     .catch((err) => console.log(err));
  // }
  function alterarInputNome(cadeira, value) {
    const assentosAlterados = assentosSelecionados.map((assento) => {
      if (assento.id == cadeira.id) {
        assento.nomeOcupante = value;
      }
      return assento;
    })
    setAssentosSelecionados(assentosAlterados);
  }

  function alterarInputCpf(cadeira, value) {
    const assentosAlterados = assentosSelecionados.map((assento) => {
      if (assento.id == cadeira.id) {
        assento.cpfOcupante = value;
      }
      return assento;
    })
    setAssentosSelecionados(assentosAlterados);
  }

  function selectChair(idAssento) {

    const assentosModificados = assentos.map((assento) => {
      if (assento.id == idAssento) {
        assento.selecionado ? assento.selecionado = false : assento.selecionado = true
      }
      return assento
    })

    setAssentos(assentosModificados)
  }

  function removeSelected(idAssento) {
    const selecionadoAlterado = assentosSelecionado.map((assento) => {
      if (assento.id != idAssento) {
        return assento;
      }
    })
    setAssentosSelecionados(selecionadoAlterado)
  }

  function assentoTipo(tipoDoAssento, numeroDoAssento) {
    let assento = ""
    switch (tipoDoAssento) {
      case 1:
        assento = "A"
        break;
      case 2:
        assento = "B"
        break;
      case 3:
        assento = "C"
        break;
      case 4:
        assento = "D"
        break;
      case 5:
        assento = "E"
        break;
      case 6:
        assento = "F"
        break;
      case 7:
        assento = "G"
        break;
      case 8:
        assento = "H"
        break;
      case 9:
        assento = "I"
        break;
      case 10:
        assento = "J"
        break;
      default:
        break;
    }

    return (`${assento}${numeroDoAssento}`)

  }

  // if (tipoDoAssento.fileira == 1){
  //   let fileiro = ""
  // }
  return (
    <>
      <Header />

      <main className="main_assentos">

        <aside>
          <section className="sessaoFilme">
            <div className="containerMovie">
              <img
                //Obs: Se o nome for muito grande, a imagem é alongada para acompanhar, mas não devia.
                src={filme.urlImagem}
                alt="besouroAzul"
              />

              <div className="infosFilme">
                <h3> {filme.titulo} </h3>
                <div className="buttonsInfos">
                  <button>{verTipo(tipo)}</button>
                  <button>{horario}</button>
                </div>
              </div>
            </div>
          </section>

          <section className="assentos">
            <div className="assentosSelecionados">


              <form action="" className="assentoSelecionado">
                {assentosSelecionados.map((assentoRes) => {
                  return (assentoRes.selecionado && <div className="selecionado" key={assentoRes.id}>
                    <div className="assentoNome">
                      <h2 className="tipoAssento">{assentoTipo(assentoRes.fileira, assentoRes.numero)}</h2>
                      <hr />
                      <h3 placeholder="Nome">Nome*</h3>
                      <input value={assentoRes.nome} onChange={(e) => alterarInputNome(assentoRes, e.target.value)} type="text" />
                      <h3>CPF*</h3>
                      <input value={assentoRes.cpf} onChange={(e) => alterarInputCpf(assentoRes, e.target.value)} type="text" />
                    </div>
                  </div>)
                })}
              </form>
            </div>
            <button onClick={(e) => modalCheckoutActivate(e)}>CONFIMAR</button>
          </section>
        </aside>



        <section className="disposicaoDosLugares">

          <div className="titulo">
            <h2>TELA</h2>
          </div>

          <div className="background">

            <div className="lugares">
              <div className="letrasEsq">
                <h5>A</h5>
                <h5>B</h5>
                <h5>C</h5>
                <h5>D</h5>
                <h5>E</h5>
                <h5>F</h5>
                <h5>G</h5>
                <h5>H</h5>
                <h5>I</h5>
                <h5>J</h5>
              </div>

              <div className="assentos_grid">
                {assentos.map((assento) => {
                  return assento.comprado ? <button className="buyBtn" key={assento.id}>{assentoTipo(assento.fileira, assento.numero)}</button> :
                    assento.selecionado ? <button className="verBtn" onClick={() => [selectChair(assento.id), removeSelected(assento.id)]} key={assento.id}>{assentoTipo(assento.fileira, assento.numero)}</button> :
                      <button onClick={() => [selectChair(assento.id), setAssentosSelecionados([...assentosSelecionados, assento])]} key={assento.id}>{assentoTipo(assento.fileira, assento.numero)}</button>
                })}
              </div>

              <div className="letrasDir">
                <h5>A</h5>
                <h5>B</h5>
                <h5>C</h5>
                <h5>D</h5>
                <h5>E</h5>
                <h5>F</h5>
                <h5>G</h5>
                <h5>H</h5>
                <h5>I</h5>
                <h5>J</h5>
              </div>
            </div>
          </div>

          <div className="legenda">
            <h5>Legenda</h5>
            <div className="botoes_legenda">
              <div className="btn">
                <button id="btn_1"></button>
                <h5>Disponível</h5>
              </div>
              <div className="btn">
                <button id="btn_2"></button>
                <h5>Selecionado</h5>
              </div>
              <div className="btn">
                <button id="btn_3"></button>
                <h5>Comprado</h5>
              </div>
            </div>
          </div>
        </section>

        <dialog className="modalCheckout">
          <div className="modalContentCheckout">
            <h2>Confirmação de Reserva!</h2>
            <h4>Tem certeza de que deseja confirmar a reserva?</h4>
            <div className="modalCheckoutBttns">
              <button className="closeModalCheckout" onClick={modalCheckoutClose}>CANCELAR</button>
              <button className="confirmModalCheckout" onClick={modalCheckoutConcluded}>CONFIRMAR</button>
            </div>
          </div>
        </dialog>

        <dialog className="modalConcluded">
          <div className="modalConcludedBttn">
            <button onClick={modalCheckoutConcludedClose}></button>
          </div>
          <div className="modalConcludedContent">
            <h2>Reserva Confirmada!</h2>
            <h4>Sua reserva foi confirmada com sucesso para a sessão selecionada.</h4>
            <h5>Estamos felizes em tê-lo conosco para essa experiência cinematográfica. Prepare-se para se envolver em uma jornada emocionante na tela grande!</h5>
          </div>
        </dialog>
      </main>
      <Footer />
    </>
  )
}

