import { useState, useEfect } from "react";

import styles from "../layouts/faleConosco.module.css";
import { Header } from "../layouts/Header";
import { Footer } from "../layouts/Footer";

export function FaleConosco(props) {
  const [nomeCompleto, setNomeCompleto] = useState("");
  const [assunto, setAssunto] = useState("");
  const [descricao, setDescricao] = useState("");

  const [mensagensRecebidas, setMensagensRecebidas] = useState([]);

  function handleClick(e) {
    e.preventDefault();

    const faleConosco = {
      nomeCompleto,
      assunto,
      descricao,
    };

    setMensagensRecebidas([...mensagensRecebidas, faleConosco]);
  }

  return (
    <>
      <Header />
      <section className={styles.pageBody}>
        <div className={styles.contactCard}>
          <h1 className={styles.title}>Contato</h1>
          <h3 className={styles.mensage}>Encontrou algum problema?</h3>
          <h3 className={styles.mensage}>Envie uma mensagem!</h3>
          <input
            type="text"
            placeholder="Nome Completo"
            onChange={(e) => setNomeCompleto(e.target.value)}
            className={styles.inputName}
          />
          <input
            type="text"
            placeholder="Assunto"
            onChange={(e) => setAssunto(e.target.value)}
            className={styles.inputName}
          />
          <textarea
            name=""
            id=""
            placeholder="Descrição Detalhada"
            onChange={(e) => setDescricao(e.target.value)}
          ></textarea>
          <button className={styles.btnSendFaleConosco} onClick={(e) => handleClick(e) }>ENVIAR</button>
        </div>
      </section>
      <Footer />
    </>
  );
}
