import "../layouts/MostrarSessoesTESTEjs.css";
import { Header } from "../layouts/Header";
import { Footer } from "../layouts/Footer";
import { Router } from "react-router-dom";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import rect15 from "../assets/rect15.svg";

export function MostrarSessoesTESTEjs(props) {
  const { id } = useParams();

  const [filme, setFilme] = useState([]);
  const [filmes, setFilmes] = useState([]);

  const [sessoes, setsessoes] = useState([]);

  const [titulo, setTitulo] = useState();
  const [horario, setHorario] = useState();
  const [tipo, setTipo] = useState();
  const [urlImage, setUrlImage] = useState();

  function handleRedirect(e, sessaoId, filmeId, tipo, horario) {
    e.preventDefault();
    console.log(sessaoId, filmeId, tipo, horario);
    window.location.assign(`http://localhost:5173/checkout/${sessaoId}/${filmeId}/${tipo}/ ${horario}`);
  }

  useEffect(() => {
    fetch("http://localhost:8000/movie", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      }
    })
      .then((resp) => resp.json())
      .then((data) => {
        setFilmes(data);
      })
      .catch((err) => console.log(err))
      .catch((err) => console.log(err));
  }, [])

  useEffect(() => {
    fetch(`http://localhost:8000/movie/${id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    }).then((bruteData) => {
      bruteData
        .json()
        .then((data) => {
          // // console.log(data);

          setFilme(data);
        })
        .catch((error) => {
          console.log(error);
        })
        .catch((error) => {
          console.log(error);
        });
    });
  }, []);

  useEffect(() => {
    fetch(`http://localhost:8000/session/search/${id}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
    }).then((bruteData) => {
      bruteData
        .json()
        .then((data) => {
          // filtredSessions(data);
          setsessoes(data);
        })
        .catch((error) => {
          console.log(error);
        })
        .catch((error) => {
          console.log(error);
        });
    });
  }, []);


  var active2D = false;
  var active3D = false;
  var activeIMAX = false;


  function handleClick2D() {
    if (active2D == false) {
      active2D = true
      console.log(active2D);
      document.getElementById("2d").style.display = 'block'
      document.getElementById("btn2D").style.backgroundColor = '#0066CC'
      document.getElementById("btn2D").style.color = '#ffffff'
      if (!active3D) {
        document.getElementById("3d").style.display = 'none'
      }
      if (!activeIMAX) {
        document.getElementById("imax").style.display = 'none'
      }
    }
    else {
      active2D = false
      console.log(active2D);
      document.getElementById("2d").style.display = 'none'
      document.getElementById("btn2D").style.background = 'none'
      document.getElementById("btn2D").style.color = '#0066CC'
    }
  }

  function handleClick3D() {
    if (!active3D) {
      active3D = true
      console.log(active3D);
      document.getElementById("3d").style.display = 'block'
      document.getElementById("btn3D").style.backgroundColor = '#0066CC'
      document.getElementById("btn3D").style.color = '#ffffff'
      if (!active2D) {
        document.getElementById("2d").style.display = 'none'
      }
      if (!activeIMAX) {
        document.getElementById("imax").style.display = 'none'
      }
    }
    else {
      active3D = false
      document.getElementById("3d").style.display = 'none'
      document.getElementById("btn3D").style.background = 'none'
      document.getElementById("btn3D").style.color = '#0066CC'
    }
  }

  function handleClickIMAX() {
    if (!activeIMAX) {
      activeIMAX = true
      console.log(activeIMAX);
      document.getElementById("imax").style.display = 'block'
      document.getElementById("btnIMAX").style.backgroundColor = '#0066CC'
      document.getElementById("btnIMAX").style.color = '#ffffff'
      if (!active2D) {
        document.getElementById("2d").style.display = 'none'
      }
      if (!active3D) {
        document.getElementById("3d").style.display = 'none'
      }
    }
    else {
      activeIMAX = false
      document.getElementById("imax").style.display = 'none'
      document.getElementById("btnIMAX").style.background = 'none'
      document.getElementById("btnIMAX").style.color = '#0066CC'
    }
  }

  async function handleChange(e) {
    // FAZER UMA FUNCAO PARA SUMIR AS OS TIPOS DE SESSAO QUE NãO TEM NADA
    let entrouFetch = false
    if (e.target.id == "cidade") {
      let city = e.target.id
      entrouFetch = true
      await fetch(`http://localhost:8000/session/search/${filme.id}`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          cidade: e.target.value
        })
      }).then((bruteData) => {
        bruteData.json().then((data) => {
          setsessoes(data)
        }).catch((err) => {
          console.log(err)
        }).catch((err) => {
          console.log(err)
        })
      })
    }

    if (e.target.id == "bairro") {
      entrouFetch = true
      let bairro = e.target.id
      await fetch(`http://localhost:8000/session/search/${filme.id}`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          bairro: e.target.value
        })
      }).then((bruteData) => {
        bruteData.json().then((data) => {
          setsessoes(data)
        }).catch((err) => {
          console.log(err)
        }).catch((err) => {
          console.log(err)
        })
      })
    }

    // Mesclas os selects e fazer com que as divs das sessoes sumam caso não tenham nenhum elemento

  }

  let temSessoes0 = sessoes.filter(sessao => sessao.tipo == 0)
  let temSessoes1 = sessoes.filter(sessao => sessao.tipo == 1)
  let temSessoes2 = sessoes.filter(sessao => sessao.tipo == 2)

  var cidades = sessoes.map(sessao => sessao.cidade)
  var cidadesEscolhidas = []

  var bairros = sessoes.map(sessao => sessao.bairro)
  var bairrosEscolhidos = []

  return (
    <>
      <Header />

      <section className="sessoesMain">
        <div className="filmeSection">
          <img className="image" src={filme.urlImagem} alt="besousroAzul" />

          <div className="moviesInfo">
            <div className="classificacaoEtaria">
              <h1>{filme.titulo}</h1>
              {(() => {
                if (filme.classificacao == 0) {
                  return (
                    <img
                      src="https://blogdojotace.com.br/wp-content/uploads/2013/06/L.png"
                      className="img_classificacao"
                    ></img>
                  );
                } else if (filme.classificacao == 1) {
                  return (
                    <img
                      src="https://img.olhardigital.com.br/wp-content/uploads/2022/02/NR10-AUTO.jpg"
                      className="img_classificacao"
                    ></img>
                  );
                } else if (filme.classificacao == 2) {
                  return (
                    <img
                      src="https://img.olhardigital.com.br/wp-content/uploads/2022/02/NR12-AUTO.jpg"
                      className="img_classificacao"
                    ></img>
                  );
                } else if (filme.classificacao == 3) {
                  return (
                    <img
                      src="https://img.olhardigital.com.br/wp-content/uploads/2022/02/NR14-AUTO.jpg"
                      className="img_classificacao"
                    ></img>
                  );
                } else if (filme.classificacao == 4) {
                  return (
                    <img
                      src="https://img.olhardigital.com.br/wp-content/uploads/2022/02/NR16-AUTO.jpg"
                      className="img_classificacao"
                    ></img>
                  );
                } else if (filme.classificacao == 5) {
                  return (
                    <img
                      src="https://img.olhardigital.com.br/wp-content/uploads/2022/02/NR18-AUTO.jpg"
                      className="img_classificacao"
                    ></img>
                  );
                }
              })()}
            </div >
            <h2>{filme.genero}</h2>
            <h2>{filme.sinopse}</h2>

            <div className="selectArea">
              <div className="styleForFilters">
                <select
                  name="placeChoose"
                  defaultValue="Cidade"
                  className="placeChoose"
                  onChange={(e) => handleChange(e)}
                  id="cidade"
                >

                  <option value="">Cidade</option>
                  <option value="Niterói">Niterói</option>
                  <option value="Itaboraí">Itaboraí</option>
                  <option value="São Gonçalo">São Gonçalo</option>

                  {/* ------------------------------------------------------- */}
                </select>

                <div className="styleForFilters">
                  <select
                    name="placeChoose"
                    defaultValue="Bairro"
                    className="placeChoose"
                    onChange={(e) => handleChange(e)}
                    id="bairro"
                  >
                    <option value="">Bairro</option>
                    <option value="Alcântara">Alcântara</option>
                    <option value="Icaraí">Icaraí</option>
                    <option value="Ampliação">Ampliação</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="clockSession">
          <div className="sessionType">
            <button
              id="btn2D"
              onClick={(e) => {
                handleClick2D(e);
              }}
            >
              2D
            </button>
            <button
              id="btn3D"
              onClick={(e) => {
                handleClick3D(e);
              }}
            >
              3D
            </button>
            <button
              id="btnIMAX"
              onClick={(e) => {
                handleClickIMAX(e);
              }}
            >
              IMAX
            </button>
          </div>

          <div className="sessionTime" id="2d">
            <h3>2D</h3>

            <div className="clocks">
              {(() => {
                if (temSessoes0.length == 0) {
                  return (
                    <button>Não há sessoes disponíveis</button>
                  )
                } else {
                  return (
                    sessoes.filter(sessao => sessao.tipo == 0).map(sessaoFiltrada => (
                      <button onClick={(e) => { handleRedirect(e, sessaoFiltrada.id, sessaoFiltrada.cineminha, sessaoFiltrada.tipo, sessaoFiltrada.horario) }} key={sessaoFiltrada.id}>{sessaoFiltrada.horario}</button>
                    )))
                }
              })()}
            </div>
          </div>

          <div className="sessionTime" id="3d">
            <div>
              <h3>3D</h3>
            </div>

            <div className="clocks">
              {(() => {
                if (temSessoes1.length == 0) {
                  return (
                    <button>Não há sessoes disponíveis</button>
                  )
                } else {
                  return (
                    sessoes.filter(sessao => sessao.tipo == 1).map(sessaoFiltrada => (
                      <button onClick={(e) => { handleRedirect(e, sessaoFiltrada.id, sessaoFiltrada.cineminha, sessaoFiltrada.tipo, sessaoFiltrada.horario) }} key={sessaoFiltrada.id}>{sessaoFiltrada.horario}</button>
                    )))
                }
              })()}
            </div>
          </div>

          <div className="sessionTime" id="imax">
            <div>
              <h3>IMAX</h3>
            </div>

            <div className="clocks">
              {(() => {
                if (temSessoes2.length == 0) {
                  return (
                    <button>Não há sessoes disponíveis</button>
                  )
                } else {
                  return (
                    sessoes.filter(sessao => sessao.tipo == 2).map(sessaoFiltrada => (
                      <button onClick={(e) => { handleRedirect(e, sessaoFiltrada.id, sessaoFiltrada.cineminha, sessaoFiltrada.tipo, sessaoFiltrada.horario) }} key={sessaoFiltrada.id}>{sessaoFiltrada.horario}</button>
                    )))
                }
              })()}
            </div>
          </div>
        </div>
      </section>

      <Footer />
    </>
  );
}

