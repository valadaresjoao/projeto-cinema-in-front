import { FaleConosco } from "./pages/faleConosco";
import "./global.css";
import { Registro } from "./pages/registro";
import Home from "./pages/home";
import { Login } from "./pages/login";
import { Filmes } from "./pages/filmes";
import { Carousel } from "./components/carousel";
import {BrowserRouter,Route, Routes} from 'react-router-dom'
import { MostrarSessoesTESTEjs } from "./pages/MostrarSessoesTESTEjs";
import { Checkout } from "./pages/Checkout";
function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/Login" element={<Login />} />
        <Route path="/Filmes" element={<Filmes />} />
        <Route path="/Registro" element={<Registro />} />
        <Route path="/FaleConosco" element={<FaleConosco />} />
        <Route
          path="/MostrarSessoesTESTEjs/:id"
          element={<MostrarSessoesTESTEjs />}
        />

        <Route path="/Checkout/:sessaoId/:filmeId/:tipo/:horario" element={<Checkout />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
