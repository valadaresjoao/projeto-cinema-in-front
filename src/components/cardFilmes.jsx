import React, { useState, useEffect } from "react";
import styles from "../layouts/filmes.module.css"
export function CardFilmes(){
const [filmes, setFilmes] = useState([]);

function handleRedirect(e, id) {
    e.preventDefault();
    console.log(id);
    window.location.assign(`http://localhost:5173/MostrarSessoesTESTEjs/${id}`);
  }
  useEffect(() => {
    fetch("http://localhost:8000/movie", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((resp) => resp.json())
      .then((data) => {
        setFilmes(data);
      })
      .catch((err) => console.log(err))
      .catch((err) => console.log(err));
  }, []);

  async function setarFilmes(data) {
    for (let i = 0; i <= 5; i++) {
      setFilmes(data[i]);
    }

    console.log(filmes);
  }
  function handleSession(e, id) {
    e.preventDefault();
    redirect("/MostrarSessoesTESTEjs");
  }

    return(
        <>
        <main className={styles.mainFilmes}>
          <h2 className={styles.filmesTitle}>Filmes</h2>
          <section className={styles.menu}>
            {filmes.map((filme) => {
              return (
                <div className={styles.staticCard} key={filme.id}>
                  <div className={styles.imageCard}>
                    <img src={filme.urlImagem} alt="filme" />
                  </div>
                  <div className={styles.nameAndClassification}>
                    <h3>{filme.titulo}</h3>
                    {(() => {
                      if (filme.classificacao == 0) {
                        return (
                          <img
                            src="https://blogdojotace.com.br/wp-content/uploads/2013/06/L.png"
                            className={styles.img_classificacao}
                          ></img>
                        );
                      } else if (filme.classificacao == 1) {
                        return (
                          <img
                            src="https://img.olhardigital.com.br/wp-content/uploads/2022/02/NR10-AUTO.jpg"
                            className={styles.img_classificacao}
                          ></img>
                        );
                      } else if (filme.classificacao == 2) {
                        return (
                          <img
                            src="https://img.olhardigital.com.br/wp-content/uploads/2022/02/NR12-AUTO.jpg"
                            className={styles.img_classificacao}
                          ></img>
                        );
                      } else if (filme.classificacao == 3) {
                        return (
                          <img
                            src="https://img.olhardigital.com.br/wp-content/uploads/2022/02/NR14-AUTO.jpg"
                            className={styles.img_classificacao}
                          ></img>
                        );
                      } else if (filme.classificacao == 4) {
                        return (
                          <img
                            src="https://img.olhardigital.com.br/wp-content/uploads/2022/02/NR16-AUTO.jpg"
                            className={styles.img_classificacao}
                          ></img>
                        );
                      } else if (filme.classificacao == 5) {
                        return (
                          <img
                            src="https://img.olhardigital.com.br/wp-content/uploads/2022/02/NR18-AUTO.jpg"
                            className={styles.img_classificacao}
                          ></img>
                        );
                      }
                    })()}
                  </div>
                  <div className={styles.filmInfo}>
                    <h5>{filme.genero}</h5>
                    <h5>Direção</h5>
                    <h5>Descrição: {filme.sinopse}</h5>
                  </div>
                  <button
                    className={styles.seeSection}
                    onClick={(e) => handleRedirect(e, filme.id)}
                  >
                    VER SESSÕES
                  </button>
                </div>
              );
            })}
          </section>
        </main>
        </>
    )
}