import "./Main.css";
import MoviesReady from "../MoviesReady";
import Introduction from "../Introduction";
import { Carousel } from "../carousel"

const Main = (props) => {
  return (
    <main className="main">
      <Introduction></Introduction>
      <Carousel/>
      <MoviesReady dado={props.dado}></MoviesReady>
    </main>
  );
};

export default Main;
