import "./Header.css";
import IconeIngresso from "../Assets/Icone-Ingresso.svg";
import IconeFilmes from "../Assets/Icone-Filmes.svg";
import IconeEntrar from "../Assets/Icone-Entrar.svg";
import IconeAjuda from "../Assets/Icone-Ajuda.svg";
import {NavLink} from  'react-router-dom'
export function Header(props) {
  const nometeste = 'C{IN}EMA'

  function mostraUsuario(usuario) {
    if (usuario){


      return <h1>Olá {usuario.nome}</h1>
    }
  }

  return (

    <section className="header_section">
      <div className="images_section">
      <div className="btnLogo"> 
      <button className="btnHeader"><NavLink to='/'><img src={IconeIngresso} alt="ingresso"></img></NavLink></button>
      <button className="btnHeader"><NavLink to='/'><button className="btnHeader h3">{nometeste}</button></NavLink></button>
      </div>
      </div>
      <div>{mostraUsuario(props.usuario)}</div>
      <div className="icons_header">
        
          <button className="btnHeader"><NavLink to='/Filmes'><img src={IconeFilmes} alt="ingresso"/></NavLink></button>
          <button className="btnHeader"><NavLink to='/Login'><img src={IconeEntrar} alt=""></img></NavLink></button>
          <button className="btnHeader"><NavLink to='/FaleConosco'><img src={IconeAjuda} alt=""></img></NavLink></button>
          
      </div>
    </section>
  );
};