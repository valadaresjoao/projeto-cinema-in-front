import { Footer } from "../layouts/Footer";
import { Header } from "../layouts/Header";
import styles from "../layouts/filmes.module.css"
import rect15 from "../assets/rect15.svg"
import React, { useState, useEffect } from "react";
export function Filmes() {
  const itemsPerPage = 10; // Defina quantos itens você quer exibir por página
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [allItems, setAllItems] = useState([]);
  const [itemsToDisplay, setItemsToDisplay] = useState([]);
  const [filmes, setFilmes] = useState([]);
  const [pesquisaFilme, setPesquisaFilme] = useState("");
  const [pesquisaGenero, setPesquisaGenero] = useState("");
  const [pesquisaClassificacao, setPesquisaClassificacao] = useState("");

  useEffect(() => {
    fetch("http://localhost:8000/movie", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((resp) => resp.json())
      .then((data) => {
        setAllItems(data); // Supondo que data é um array com todos os itens
        setTotalPages(Math.ceil(data.length / itemsPerPage));
        console.log(data)
      })
      .catch((err) => console.log(err))
      .catch((err) => console.log(err));
  }, []);
  useEffect(() => {
    const startIndex = (currentPage - 1) * itemsPerPage;
    const endIndex = startIndex + itemsPerPage;
    const itemsOnPage = allItems.slice(startIndex, endIndex);
    setFilmes(itemsOnPage);
  }, [currentPage, allItems]);

  const handlePageChange = newPage => {
    setCurrentPage(newPage);
  };

    console.log(filmes);
  

  function handleClick(e) {
    e.preventDefault();

    console.log(pesquisaFilme);
    console.log(pesquisaGenero);
    console.log(pesquisaClassificacao);

    fetch("http://localhost:8000/movie/search", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        titulo: pesquisaFilme,
        genero: pesquisaGenero,
        classificacao: pesquisaClassificacao,
      }),
    })
      .then((resp) => resp.json())
      .then((data) => {
        if (data.length != 0){
        setFilmes(data);
        }else{
        setFilmes([{titulo: "FILME NÃO ENCONTRADO", urlImagem: "https://media.istockphoto.com/id/1265221960/pt/vetorial/page-not-found-error-with-film-flap-design.jpg?s=1024x1024&w=is&k=20&c=yuP2ixJFR1-ybM8_S_kgnRSendLf4S_6dyBFI5rfGVk="}])
        }
      })
      .catch((err) => console.log(err))
      .catch((err) => console.log(err));
  }

  function handleRedirect(e, id) {
    e.preventDefault();
    console.log(id);
    window.location.assign(`http://localhost:5173/MostrarSessoesTESTEjs/${id}`);
  }


  return (
    <>
      <Header />
      <section className={styles.bodyPageFilmes}>
        <section className={styles.searchArea}>
          <div className={styles.searchBarAlign}>
            <input
              type="text"
              className={styles.searchBar}
              placeholder="Clique na lupa para pesquisar filmes e filtros"
              onChange={(e) => setPesquisaFilme(e.target.value)}
            />
            <button
              className={styles.submitSearch}
              onClick={(e) => handleClick(e)}
            ></button>
          </div>
          <div className={styles.filters}>
            <img src={rect15} alt="" className={styles.filterStyleFigure} />
            <select
              name=""
              id=""
              className={styles.typeFilter}
              onChange={(e) => setPesquisaGenero(e.target.value)}
            >

              <option value="">Gênero</option>
              <option value="Ação">Ação</option>
              <option value="Terror">Terror</option>
              <option value="Suspense">Suspense</option>
              <option value="Mistério">Mistério</option>
              <option value="Romance">Romance</option>
              <option value="Drama">Drama</option>
              <option value="Animação">Animação</option>
              <option value="Comédia">Comédia</option>

              {/* ------------------------------------------------------ */}

            </select>
            <img src={rect15} alt="" className={styles.filterStyleFigure} />
            <select
              name=""
              id=""
              className={styles.typeFilter}
              onChange={(e) => setPesquisaClassificacao(e.target.value)}
            >
              {/* -------------------------- */}
              <option value="">Classificação</option>
              <option value="0">Livre</option>
              <option value="1">10</option>
              <option value="2">12</option>
              <option value="3">14</option>
              <option value="4">16</option>
              <option value="5">18</option>

            </select>
          </div>
        </section>
        <h2 className={styles.filmesTitle}>Filmes</h2>
    <main className={styles.mainFilmes}>
        {filmes.map(filme => (
        <section className={styles.menu}>

        <div className={styles.staticCard}>
            <div className={styles.imageCard}>
            <img src={filme.urlImagem}/>
            </div>
        <div className={styles.nameAndClassification}>
           
            <h3>{filme.titulo}</h3>
            {(() => {
                      if (filme.classificacao == 0) {
                        return (
                          <img
                            src="https://blogdojotace.com.br/wp-content/uploads/2013/06/L.png"
                            className={styles.img_classificacao}
                          ></img>
                        );
                      } else if (filme.classificacao == 1) {
                        return (
                          <img
                            src="https://img.olhardigital.com.br/wp-content/uploads/2022/02/NR10-AUTO.jpg"
                            className={styles.img_classificacao}
                          ></img>
                        );
                      } else if (filme.classificacao == 2) {
                        return (
                          <img
                            src="https://img.olhardigital.com.br/wp-content/uploads/2022/02/NR12-AUTO.jpg"
                            className={styles.img_classificacao}
                          ></img>
                        );
                      } else if (filme.classificacao == 3) {
                        return (
                          <img
                            src="https://img.olhardigital.com.br/wp-content/uploads/2022/02/NR14-AUTO.jpg"
                            className={styles.img_classificacao}
                          ></img>
                        );
                      } else if (filme.classificacao == 4) {
                        return (
                          <img
                            src="https://img.olhardigital.com.br/wp-content/uploads/2022/02/NR16-AUTO.jpg"
                            className={styles.img_classificacao}
                          ></img>
                        );
                      } else if (filme.classificacao == 5) {
                        return (
                          <img
                            src="https://img.olhardigital.com.br/wp-content/uploads/2022/02/NR18-AUTO.jpg"
                            className={styles.img_classificacao}
                          ></img>
                        );
                      }
                    })()}
                    </div>
                    <div className={styles.filmInfo}>
                    <h5>{filme.genero}</h5>
                    <h5>Direção: {filme.direcao}</h5>
                    <h5>Descrição: {filme.sinopse}</h5>
                    </div>
                  <button
                    className={styles.seeSection}
                    onClick={(e) => handleRedirect(e, filme.id)}>
                    VER SESSÕES
                  </button>

        </div>
        </section>

        ))}
        
      <div>
        
      </div>
      </main>
      </section>
      <div className={styles.paginationBttns}>
   
        <button
        className={styles.bttnPrevious}
          onClick={() => handlePageChange(currentPage - 1)}
          disabled={currentPage === 1}
        >
          <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" fill="#fcfcfc" viewBox="0 0 256 256"><path d="M165.66,202.34a8,8,0,0,1-11.32,11.32l-80-80a8,8,0,0,1,0-11.32l80-80a8,8,0,0,1,11.32,11.32L91.31,128Z"></path></svg>
        </button>
        {
        Array(totalPages).fill('').map((_, index) => {
          return <button className={styles.numberBttns}
          key={index}
           onClick={() => setCurrentPage(index + 1)}
           disabled={index === currentPage - 1}
           >
            {index + 1} 
            </button>
        })
      }
        
        <button
         className={styles.bttnNext}
          onClick={() => handlePageChange(currentPage + 1)}
          disabled={currentPage === totalPages}
        >
          <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" fill="#fcfcfc" viewBox="0 0 256 256"><path d="M181.66,133.66l-80,80a8,8,0,0,1-11.32-11.32L164.69,128,90.34,53.66a8,8,0,0,1,11.32-11.32l80,80A8,8,0,0,1,181.66,133.66Z"></path></svg>
        </button>
    </div>
      <Footer />
    </>
  );
}